import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Material
import Material.Button as Button
import Material.Textfield as Textfield
import Material.Table as Table
import Material.Icon as Icon
import Material.Options as Options exposing (css)

main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

type alias Model =
  { message : String
  , index : Int
  , list : List Int
  , mdl : Material.Model
  }

init : (Model, Cmd Msg)
init =
  (Model "" 0 [] Material.model, Cmd.none)

type Msg
  = Input String 
  | MyClickMsg
  | Mdl (Material.Msg Msg)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Input str->
        ({model | message = str}, Cmd.none)
    MyClickMsg ->
        let
            index = model.index + 1
        in           
            ({model | index = index, list = (List.append model.list [index])}, Cmd.none)
    Mdl msg_ ->
            Material.update Mdl msg_ model
    
subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none

view : Model -> Html Msg
view model =
    div
        [style [ ( "padding", "2rem" ) ]]
        [ div [divStyle] [ 
            div[]
                [ Textfield.render Mdl [0] model.mdl               
                    [ Textfield.floatingLabel
                    , Options.onInput Input
                    , Textfield.textarea
                    , Textfield.rows 6
                    ]
                    []
                ]
            ,div[]
                ((indexList model.message) |> List.map (\item ->
                   Textfield.render Mdl [item] model.mdl               
                        [ Textfield.floatingLabel
                        --, Options.onInput Input
                        , Textfield.textarea
                        , Textfield.rows 6
                        ]
                        []
                   )
                )           
            ,div[]
                [ Button.render Mdl [-1] model.mdl
                    [ Button.fab
                    , Button.colored
                    , Button.ripple
                    , Options.onClick MyClickMsg
                    ]
                    [ Icon.i "add"]
                ]
            ,div[][text (toString model.message)]
            ,div[][text (toString (indexList model.message))]
            ]        
        ]
              
indexList str = 
    let 
        x = String.lines (String.toUpper str)
    in
        (
            (
                (List.filterMap delNullValue x) 
                |> List.map (\item ->
                    String.split " " item
                    |> List.filterMap delNullValue
                    |> screening 
                    )
            )
            |> List.filterMap delNullValue
            |> List.map screeningCallNum
        )
        |> List.foldl delRepeat [] 

screening list =
    let
        msg = String.concat list
        first = String.left 4 msg
    in
        if first == "CALL" && List.length list == 2 then
            let
                second = String.dropLeft 4 msg
                x = String.toInt second
            in
                case x of
                    Ok y ->
                        second
                    Err _ ->
                        ""
        else
            ""

screeningCallNum : String -> Int
screeningCallNum str =
    let
        x = String.toInt str           
    in
        case x of
            Ok y ->
                y
            Err _ ->
                -1
                
delRepeat : Int -> List Int -> List Int
delRepeat num list =
    if isNaN (toFloat num) then    
        list
    else
        if (List.member num list) then
            list
        else
            list ++ [num]

delNullValue : String -> Maybe String
delNullValue str =
    if str == "" then
        Nothing
    else
        Just str

divStyle = 
  style 
   [ ("float","left")
    ,("margin","10px")
   ]


