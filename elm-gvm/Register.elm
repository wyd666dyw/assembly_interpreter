module Register exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Material.Table as Table

type alias Model = 
    { ax :HL
    , bx : HL
    , cx : HL
    , dx : HL
    , cs : Int
    , ip : Int
    , ss : Int
    , sp : Int
    , bp : Int
    , si : Int
    , di : Int
    , ds : Int
    , es : Int
    , err : String
    }

type alias HL =
    { h : String
    , l : String
    }
init =
  (Model (HL "00" "00") (HL "00" "00") (HL "00" "00") (HL "00" "00") 0 0 0 0 0 0 0 0 0 "" )

view model =
    div
        [style [ ("padding", "2rem") ]]
        [ div[divStyle] [    
                Table.table []
                    [ Table.thead []
                      [ Table.tr []
                        [ Table.th [ Table.numeric ] [ text "Register" ]
                        , Table.th [ Table.numeric ] [ text "H" ]
                        , Table.th [ Table.numeric ] [ text "L" ]
                        ]
                      ]
                    , Table.tbody []
                        ((data model)|> List.map (\item ->
                           Table.tr []
                             [ Table.td [ Table.numeric ] [ text item.register ]
                             , Table.td [ Table.numeric ] [ text item.highBit ]
                             , Table.td [ Table.numeric ] [ text item.lowBit ]
                             ]
                           )
                        )
                    ]
                ,Table.table []
                  [
                     Table.tbody []
                        ((data1 model)|> List.map (\item ->
                           Table.tr []
                             [ Table.td [ Table.numeric ] [ text item.register ]
                             , Table.td [ Table.numeric ] [ text item.highBit ]
                             ]
                           )
                        )
                  ]
                ]
        ]

divStyle = 
  style 
   [  ("float","left")
     ,("box-shadow","4px 4px 10px #888")
     ,("margin","10px")
    ]

data model =
  [ { register = "AX"  , highBit = model.ax.h, lowBit = model.ax.l }
  , { register = "BX"  , highBit = model.bx.h, lowBit = model.bx.l }
  , { register = "CX"  , highBit = model.cx.h, lowBit = model.cx.l }
  , { register = "DX"  , highBit = model.dx.h, lowBit = model.dx.l }
  ]

data1 model =
  [ { register = "CS"  , highBit = toString(model.cs)}
  , { register = "IP"  , highBit = toString(model.ip)}
  , { register = "SS"  , highBit = toString(model.ss)}
  , { register = "SP"  , highBit = toString(model.sp)}
  , { register = "BP"  , highBit = toString(model.bp)}
  , { register = "SI"  , highBit = toString(model.si)}
  , { register = "DI"  , highBit = toString(model.di)}
  , { register = "DS"  , highBit = toString(model.ds)}
  , { register = "ES"  , highBit = toString(model.es)}
  ]




