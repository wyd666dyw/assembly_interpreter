module Main exposing (..)

import Html exposing (..)
import Html.Attributes exposing (class, style)
import Register
import Compile
import Memory 
import Material
import Material.Scheme
import Material.Button as Button
import Material.Textfield as Textfield
import Material.Table as Table
import Material.Icon as Icon
import Material.Menu as Menu
import Material.Options as Options exposing (css)

-- MODEL
type alias Model =
    { input : List String
    , stepNum : Int
    , runNum : Int
    , register : Register.Model
    , memory : Memory.Model
    , mdl : Material.Model
    }

init : (Model, Cmd Msg)
init =
  (Model [] -1 0 Register.init Memory.init Material.model, Cmd.none)

type Msg
    = Input String
    | ClickStep
    | ClickRun 
    | Mdl (Material.Msg Msg)

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Input str ->
            ({ model | input = (inputToList str)}
            , Cmd.none
            )
        ClickStep ->
            (gvmStepOperation model)
        ClickRun ->
            (gvmOperation model)
        Mdl msg_ ->
            Material.update Mdl msg_ model



-- VIEW
view : Model -> Html Msg
view model =
    div
        [style [ ( "padding", "2rem" ) ]]
        [ div [divStyle] [   
            Textfield.render Mdl [0] model.mdl
                  [ Textfield.floatingLabel
                  --, Textfield.value model.input
                  , Options.onInput Input
                  , Textfield.textarea
                  , Textfield.rows 6
                  ]
                  []
            ,div[]
              [ Button.render Mdl [1] model.mdl
                    [ Button.raised
                    , Button.ripple
                    , Options.onClick ClickStep
                    ]
                    [ Icon.i "skip_next",text "Step"]
                    --[icon]
              ,Button.render Mdl [2] model.mdl
                    [ Button.raised
                    , Button.ripple
                    , Options.onClick ClickRun 
                    ]
                    [Icon.i "play_arrow",text "Run"]
              ]
            ]
          ,Register.view model.register
          ,div [divStyle][text model.register.err]
          ,Memory.view  model.memory         
        ]
        |> Material.Scheme.top

gvmOperation model= 
  let
    regMer = (Compile.dealData model.register model.memory model)
  in
    if model.register.err /= "" then
      ({ model | stepNum = -1, runNum = 1, register = Register.init, memory = Memory.init}, Cmd.none)
    else
      ({ model | stepNum = -1, runNum = 1, register = (Tuple.first regMer), memory = (Tuple.second regMer)}, Cmd.none)

gvmStepOperation model= 
  if (List.isEmpty model.input) then
    if model.register.err == "" then
      ({model | stepNum = -1, runNum = 0, register = (registerERR model.register)}, Cmd.none)
    else
      ({model| stepNum = -1, runNum = 0, register = Register.init, memory = Memory.init},Cmd.none)
  else 
      let
         x = model.stepNum + 1
      in   
        if model.register.err /= "" || model.runNum == 1 || x >= (List.length model.input) then
          ({model| stepNum = -1, runNum = 0, register = Register.init, memory = Memory.init},Cmd.none)
        else
          let
            regMer = (Compile.dealStepData model.register model.memory model)
          in
            ({ model | stepNum = x, runNum = 0, register = (Tuple.first regMer), memory = (Tuple.second regMer)}, Cmd.none) 

registerERR register =
  ({register | err = "ERROR: The program terminates execution registerERR"})

inputToList str = 
    let 
        x = String.lines (String.toUpper str)
        y = List.filterMap Compile.delNullValue x
        endValue = List.head (List.reverse y)
    in
      case endValue of
        Just value ->
          if value == "RET" then
            List.take ((List.length y) - 1) y
          else
            []
        Nothing ->
          []

icon : Html m
icon = Icon.view "pause" [Icon.size24]

divStyle = 
  style 
   [ ("float","left")
    ,("margin","10px")
   ]

main =
  Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = always Sub.none
    }
