module Compile exposing (..)
import Dict
import Register
import Memory
import Hex


dealStepData register memory model =  
    let 
        dict = Dict.fromList (List.indexedMap (,) model.input) 
        step = dictValue (Dict.get (model.stepNum + 1) dict)    --Note：model.stepNum + 1
        newRegister = dealSingle step (register,memory)
    in
        (runData newRegister)
      
dealData register memory model = 
    if (List.isEmpty model.input) then
        ({register | err = "ERROR: The program terminates execution dealData"},Memory.init)
    else 
        let 
            y = List.reverse model.input
            len = List.length y
        in
            if model.runNum == 1 then
                let
                    newRegMer = List.foldr (dealSingle) (Register.init,Memory.init) y      
                in                   
                    (runData newRegMer)
            else 
                let
                    newList = List.take (len - model.stepNum - 1) y
                    newRegMer = List.foldr (dealSingle) (register,memory) newList
                in
                    (runData newRegMer)   
                
runData newRegMer = 
    let
        register = Tuple.first newRegMer
        memory = Tuple.second newRegMer        
    in       
        if register.err == "" then
            (register,memory)
        else
            let
                errValue = register.err 
                x = Register.init   
            in
                ({x | err = errValue},Memory.init)
                    
dealSingle str regMer =   
    let
        register = Tuple.first regMer
        memory = Tuple.second regMer
        list = List.filterMap delNullValue (String.split " " str)
        len = List.length list
        dict = Dict.fromList (List.indexedMap (,) list)           
    in
        if len >= 2 && len <= 3 then
            let
                first = dictValue (Dict.get 0 dict)
                second = dictValue (Dict.get 1 dict)            
            in
                if len == 2 then
                    if first == "INC" || first == "DEC" then
                        incOrDec register memory first second
                    else if first == "PUSH" then
                        if (List.member second ["AX","BX","CX","DX"]) then
                            let
                                valueList = Memory.dictKey_Value (Dict.get "SS" memory.newPush)
                            in
                                if second == "AX" then
                                    (pushDataReg register memory "SS" valueList (register.ax.h,register.ax.l))  
                                else if second == "BX" then
                                    (pushDataReg register memory "SS" valueList (register.bx.h,register.bx.l))  
                                else if second == "CX" then
                                    (pushDataReg register memory "SS" valueList (register.cx.h,register.cx.l)) 
                                else
                                    (pushDataReg register memory "SS" valueList (register.dx.h,register.dx.l))  
                                --else if second == "BP" then
                                --else if second == "CS" then
                                --else if second == "DS" then
                                --else if second == "ES" then
                                --else           
                        else 
                            if (List.member second memory.stack) then
                                ({register | err = "ERROR: The stack already exists"},Memory.init)
                            else
                                let
                                    stack = List.append memory.stack [second] 
                                    newPush = Dict.insert second [] memory.newPush        
                                in       
                                    (register,{memory | stack = stack, newPush = newPush})
                    else if first == "POP" then
                        if (List.member second ["AX","BX","CX","DX"]) then
                            let
                                valueList = Memory.dictKey_Value (Dict.get "SS" memory.newPush)      
                            in
                                if (List.isEmpty valueList) then
                                  ({register | err = "ERROR: No data in the stack"},Memory.init)  
                                else 
                                    (tupleValue register memory valueList second)        
                        else  
                            let
                                valueList = Memory.dictKey_Value (Dict.get second memory.newPush)                          
                            in
                                if (List.isEmpty valueList) then
                                    ({register | err = "ERROR: The stack does not exist or there is no element"},Memory.init)
                                else
                                    let
                                        msg = List.take  ((List.length valueList)-1) valueList  
                                        newPush = Dict.insert second msg memory.newPush      
                                    in                                                                                                                     
                                        (register,{memory | newPush = newPush}) 
                    else
                        ({register | err = "ERROR: The program terminates execution INCDECPUSHPOP"}, memory)                
                else
                    let
                        third = dictValue (Dict.get 2 dict)        
                    in
                        if first == "MOV" then
                            if second == "AX" then 
                                if third == "BX" || third == "[BX]" then
                                    let
                                        bl = register.bx.l
                                        bh = register.bx.h       
                                    in
                                        ({register | ax = (dealHL register.ax (bh,bl))},memory)
                                else if third == "CX" || third == "[CX]" then
                                    let
                                        cl = register.cx.l
                                        ch = register.cx.h       
                                    in          
                                        ({register | ax = (dealHL register.ax (ch,cl))},memory)
                                else if third == "DX" || third == "[DX]" then
                                    let
                                        dl = register.dx.l 
                                        dh = register.dx.h      
                                    in
                                        ({register | ax = (dealHL register.ax (dh,dl))},memory) 
                                else 
                                    (dealHexOrDecimal register memory second third)                                                   
                            else if second == "AH" then
                                if third == "BH" || third == "[BH]" then
                                    let
                                        bh = register.bx.h      
                                    in
                                        ({register | ax = (dealH register.ax bh)},memory)            
                                else if third == "CH" || third == "[CH]" then
                                    let
                                        ch = register.cx.h      
                                    in
                                        ({register | ax = (dealH register.ax ch)},memory)          
                                else if third == "DH" || third == "[DH]" then
                                    let
                                        dh = register.dx.h      
                                    in
                                        ({register | ax = (dealH register.ax dh)},memory)          
                                else if third == "BL" || third == "[BL]" then
                                    let
                                        bl = register.bx.l       
                                    in
                                        ({register | ax = (dealH register.ax bl)},memory)  
                                else if third == "CL" || third == "[CL]" then
                                    let
                                        cl = register.cx.l       
                                    in
                                        ({register | ax = (dealH register.ax cl)},memory) 
                                else if third == "DL" || third == "[DL]" then
                                    let
                                        dl = register.dx.l       
                                    in
                                        ({register | ax = (dealH register.ax dl)},memory) 
                                else
                                    (dealHexOrDecimal register memory second third)      
                            else if second == "AL" then
                                if third == "BL" || third == "[BL]" then
                                    let
                                        bl = register.bx.l       
                                    in
                                        ({register | ax = (dealL register.ax bl)},memory)  
                                else if third == "CL" || third == "[CL]" then
                                    let
                                        cl = register.cx.l       
                                    in
                                        ({register | ax = (dealL register.ax cl)},memory)
                                else if third == "DL" || third == "[DL]" then
                                    let
                                        dl = register.dx.l       
                                    in
                                        ({register | ax = (dealL register.ax dl)},memory) 
                                else if third == "BH" || third == "[BH]" then
                                    let
                                        bh = register.bx.h      
                                    in
                                        ({register | ax = (dealL register.ax bh)},memory) 
                                else if third == "CH" || third == "[CH]" then
                                    let
                                        ch = register.cx.h      
                                    in
                                        ({register | ax = (dealL register.ax ch)},memory) 
                                else if third == "DH" || third == "[DH]" then
                                    let
                                        dh = register.dx.h      
                                    in
                                        ({register | ax = (dealL register.ax dh)},memory) 
                                else
                                    (dealHexOrDecimal register memory second third)  
                            else if second == "BX" then
                                if third == "AX" || third == "[AX]" then
                                    let
                                        al = register.ax.l
                                        ah = register.ax.h        
                                    in
                                        ({register | bx = (dealHL register.bx (ah,al))},memory) 
                                else if third == "CX" || third == "[CX]" then
                                    let
                                        cl = register.cx.l
                                        ch = register.cx.h       
                                    in
                                        ({register | bx = (dealHL register.bx (ch,cl))},memory)
                                else if third == "DX" || third == "[DX]" then
                                    let
                                        dl = register.dx.l
                                        dh = register.dx.h        
                                    in
                                        ({register | bx = (dealHL register.bx (dh,dl))},memory)
                                else
                                    (dealHexOrDecimal register memory second third)   
                            else if second == "BH" then
                                if third == "AH" || third == "[AH]" then
                                    let
                                        ah = register.ax.h      
                                    in
                                        ({register | bx = (dealH register.bx ah)},memory)  
                                else if third == "CH" || third == "[CH]" then
                                    let
                                        ch = register.cx.h      
                                    in
                                        ({register | bx = (dealH register.bx ch)},memory)
                                else if third == "DH" || third == "[DH]" then
                                    let
                                        dh = register.dx.h      
                                    in
                                        ({register | bx = (dealH register.bx dh)},memory)  
                                else if third == "AL" || third == "[AL]" then
                                    let
                                        al = register.ax.l       
                                    in
                                        ({register | bx = (dealH register.bx al)},memory)  
                                else if third == "CL" || third == "[CL]" then
                                    let
                                        cl = register.cx.l       
                                    in
                                        ({register | bx = (dealH register.bx cl)},memory) 
                                else if third == "DL" || third == "[DL]" then
                                    let
                                        dl = register.dx.l       
                                    in
                                        ({register | bx = (dealH register.bx dl)},memory) 
                                else
                                    (dealHexOrDecimal register memory second third)   
                            else if second == "BL" then
                                if third == "AL" || third == "[AL]" then
                                    let
                                        al = register.ax.l       
                                    in
                                        ({register | bx = (dealL register.bx al)},memory)  
                                else if third == "CL" || third == "[CL]" then
                                    let
                                        cl = register.cx.l       
                                    in
                                        ({register | bx = (dealL register.bx cl)},memory) 

                                else if third == "DL" || third == "[DL]" then
                                    let
                                        dl = register.dx.l       
                                    in
                                        ({register | bx = (dealL register.bx dl)},memory)
                                else if third == "AH" || third == "[AH]" then
                                    let
                                        ah = register.ax.h      
                                    in
                                        ({register | bx = (dealL register.bx ah)},memory)  
                                else if third == "CH" || third == "[CH]" then
                                    let
                                        ch = register.cx.h      
                                    in
                                        ({register | bx = (dealL register.bx ch)},memory)
                                else if third == "DH" || third == "[DH]" then
                                    let
                                        dh = register.dx.h      
                                    in
                                        ({register | bx = (dealL register.bx dh)},memory) 
                                else
                                    (dealHexOrDecimal register memory second third) 
                            else if second == "CX" then
                                if third == "AX" || third == "[AX]" then
                                    let
                                        al = register.ax.l
                                        ah = register.ax.h       
                                    in
                                        ({register | cx = (dealHL register.cx (ah,al))},memory) 
                                else if third == "BX" || third == "[BX]" then
                                    let
                                        bl = register.bx.l
                                        bh = register.bx.h       
                                    in
                                        ({register | cx = (dealHL register.cx (bh,bl))},memory)
                                else if third == "DX" || third == "[DX]" then
                                    let
                                        dl = register.dx.l
                                        dh = register.dx.h       
                                    in
                                        ({register | cx = (dealHL register.cx (dh,dl))},memory) 
                                else
                                    (dealHexOrDecimal register memory second third) 
                            else if second == "CH" then
                                if third == "AH" || third == "[AH]" then
                                    let
                                        ah = register.ax.h      
                                    in
                                        ({register | cx = (dealH register.cx ah)},memory) 
                                else if third == "BH" || third == "[BH]" then
                                    let
                                        bh = register.bx.h      
                                    in
                                        ({register | cx = (dealH register.cx bh)},memory) 
                                else if third == "DH" || third == "[DH]" then
                                    let
                                        dh = register.dx.h      
                                    in
                                        ({register | cx = (dealH register.cx dh)},memory)
                                else if third == "AL" || third == "[AL]" then
                                    let
                                        al = register.ax.l       
                                    in
                                        ({register | cx = (dealH register.cx al)},memory)  
                                else if third == "BL" || third == "[BL]" then
                                    let
                                        bl = register.bx.l       
                                    in
                                        ({register | cx = (dealH register.cx bl)},memory) 
                                else if third == "DL" || third == "[DL]" then
                                    let
                                        dl = register.dx.l       
                                    in
                                        ({register | cx = (dealH register.cx dl)},memory)
                                else
                                    (dealHexOrDecimal register memory second third) 
                            else if second == "CL" then
                                if third == "AL" || third == "[AL]" then
                                    let
                                        al = register.ax.l       
                                    in
                                        ({register | cx = (dealL register.cx al)},memory)  
                                else if third == "BL" || third == "[BL]" then
                                    let
                                        bl = register.bx.l       
                                    in
                                        ({register | cx = (dealL register.cx bl)},memory)
                                else if third == "DL" || third == "[DL]" then
                                    let
                                        dl = register.dx.l       
                                    in
                                        ({register | cx = (dealL register.cx dl)},memory) 
                                else if third == "AH" || third == "[AH]" then
                                    let
                                        ah = register.ax.h      
                                    in
                                        ({register | cx = (dealL register.cx ah)},memory)  
                                else if third == "BH" || third == "[BH]" then
                                    let
                                        bh = register.bx.h      
                                    in
                                        ({register | cx = (dealL register.cx bh)},memory)
                                else if third == "DH" || third == "[DH]" then
                                    let
                                        dh = register.dx.h      
                                    in
                                        ({register | cx = (dealL register.cx dh)},memory)
                                else
                                    (dealHexOrDecimal register memory second third) 
                            else if second == "DX" then
                                if third == "AX" || third == "[AX]" then
                                    let
                                        al = register.ax.l 
                                        ah = register.ax.h      
                                    in
                                        ({register | dx = (dealHL register.dx (ah,al))},memory)  
                                else if third == "BX" || third == "[BX]" then
                                    let
                                        bl = register.bx.l
                                        bh = register.bx.h        
                                    in
                                        ({register | dx = (dealHL register.dx (bh,bl))},memory)
                                else if third == "CX" || third == "[CX]" then
                                    let
                                        cl = register.cx.l
                                        ch = register.cx.h       
                                    in
                                        ({register | dx = (dealHL register.dx (ch,cl))},memory)
                                else
                                    (dealHexOrDecimal register memory second third) 
                            else if second == "DH" then
                                if third == "AH" || third == "[AH]" then
                                    let
                                        ah = register.ax.h      
                                    in
                                        ({register | dx = (dealH register.dx ah)},memory) 
                                else if third == "BH" || third == "[BH]" then
                                    let
                                        bh = register.bx.h      
                                    in
                                        ({register | dx = (dealH register.dx bh)},memory) 
                                else if third == "CH" || third == "[CH]" then
                                    let
                                        ch = register.cx.h      
                                    in
                                        ({register | dx = (dealH register.dx ch)},memory)
                                else if third == "AL" || third == "[AL]" then
                                    let
                                        al = register.ax.l       
                                    in
                                        ({register | dx = (dealH register.dx al)},memory)  
                                else if third == "BL" || third == "[BL]" then
                                    let
                                        bl = register.bx.l       
                                    in
                                        ({register | dx = (dealH register.dx bl)},memory) 
                                else if third == "CL" || third == "[CL]" then
                                    let
                                        cl = register.cx.l       
                                    in
                                        ({register | dx = (dealH register.dx cl)},memory)  
                                else
                                    (dealHexOrDecimal register memory second third) 
                            else if second == "DL" then
                                if third == "AL" || third == "[AL]" then
                                    let
                                        al = register.ax.l       
                                    in
                                        ({register | dx = (dealL register.dx al)},memory)  
                                else if third == "BL" || third == "[BL]" then
                                    let
                                        bl = register.bx.l       
                                    in
                                        ({register | dx = (dealL register.dx bl)},memory) 
                                else if third == "CL" || third == "[CL]" then
                                    let
                                        cl = register.cx.l       
                                    in
                                        ({register | dx = (dealL register.dx cl)},memory)
                                else if third == "AH" || third == "[AH]" then
                                    let
                                        ah = register.ax.h      
                                    in
                                        ({register | dx = (dealL register.dx ah)},memory)  
                                else if third == "BH" || third == "[BH]" then
                                    let
                                        bh = register.bx.h      
                                    in
                                        ({register | dx = (dealL register.dx bh)},memory) 
                                else if third == "CH" || third == "[CH]" then
                                    let
                                        ch = register.cx.h      
                                    in
                                        ({register | dx = (dealL register.dx ch)},memory)
                                else
                                    (dealHexOrDecimal register memory second third) 
                            else
                                (dealHexOrDecimal register memory second third)      
                                --else
                                --    ({register | err = "ERROR: The stack does not exist"},Memory.init)

                            ----else if first == "ADD" then
                            ----else if first == "SUB" then
                            ----else if first == "MUL" then
                            ----else if first == "DIV" then
                        else
                            ({register | err = "ERROR: The program terminates execution MOV FIRST"},Memory.init)                         
        else
            ({register | err = "ERROR: The program terminates execution LENGTH > 3"},Memory.init)   
  
delNullValue : String -> Maybe String
delNullValue str =
    if str == "" then
        Nothing
    else
        Just str

dictValue msg =
    case msg of 
        Just x ->
            x
        Nothing ->
            "Nothing"
           
tupleValue register memory list reg =
    let
        msg = List.head (List.reverse list)       
    in
        case msg of
            Just x ->
                let
                    y = List.take  ((List.length list)-1) list
                    newPush = Dict.insert "SS" y memory.newPush                         
                in
                    if reg == "AX" then
                      ({register | ax = (dealHL register.ax ((Tuple.first x),(Tuple.second x)))},{memory | newPush = newPush}) 
                    else if reg == "BX" then
                        ({register | bx = (dealHL register.bx ((Tuple.first x),(Tuple.second x)))},{memory | newPush = newPush}) 
                    else if reg == "CX" then
                        ({register | cx = (dealHL register.cx ((Tuple.first x),(Tuple.second x)))},{memory | newPush = newPush})
                    else
                        ({register | dx = (dealHL register.dx ((Tuple.first x),(Tuple.second x)))},{memory | newPush = newPush})
            Nothing ->
                ({register | err = "ERROR: No data in the stack"},Memory.init)
dealL xx msg = 
    {xx | l = msg}

dealH xx msg = 
    {xx | h = msg}

dealHL xx tuple =
    { xx | h = (Tuple.first tuple),l = (Tuple.second tuple)}

incOrDec register memory first second =
    if second == "AX"  || second == "AL" then
        let
            hexString = register.ax.h ++ register.ax.l     
        in
            dealIncOrDec register memory first hexString "AX" "LB"
    else if second == "AH" then
        let
            hexString = register.ax.h      
        in
           dealIncOrDec register memory first hexString "AH" "HB"     
    else if second == "BX" || second == "BL" then
        let
            hexString = register.bx.h ++ register.bx.l     
        in
            dealIncOrDec register memory first hexString "BX" "LB" 
    else if second == "BH" then
        let
            hexString = register.bx.h      
        in
           dealIncOrDec register memory first hexString "BH" "HB"            
    else if second == "CX" || second == "CL" then
        let
            hexString = register.cx.h ++ register.cx.l     
        in
            dealIncOrDec register memory first hexString "CX" "LB" 
    else if second == "CH" then
        let
            hexString = register.cx.h      
        in
           dealIncOrDec register memory first hexString "CH" "HB"      
    else if second == "DX" || second == "DL" then
        let
            hexString = register.dx.h ++ register.dx.l     
        in
            dealIncOrDec register memory first hexString "DX" "LB" 
    else if second == "DH" then
        let
            hexString = register.dx.h      
        in
           dealIncOrDec register memory first hexString "DH" "HB"      
    else
        ({register | err = "ERROR: The program terminates execution" ++ first},Memory.init)

dealIncOrDec register memory sign hexString strReg bitSign  = 
    let
        hexToInt = Hex.fromString hexString      
    in
        case hexToInt of
            Ok x ->
                let
                    hexVal = intToHex sign x                                
                in
                    if bitSign == "HB" then
                        let 
                            highBit = String.right 2 hexVal
                        in
                            if strReg == "AH" then
                                ({register | ax = (dealH register.ax highBit)},memory)
                            else if strReg == "BH" then
                                ({register | bx = (dealH register.bx highBit)},memory)
                            else if strReg == "CH" then
                                ({register | cx = (dealH register.cx highBit)},memory)
                            else
                                ({register | dx = (dealH register.dx highBit)},memory)
                    else
                        let
                            lowBit = String.right 2 hexVal
                            highBit = String.right 2 (String.dropRight 2 hexVal)       
                        in        
                            if strReg == "AX" then
                                ({register | ax = (dealHL register.ax (highBit,lowBit))},memory)
                            else if strReg == "BX" then
                                ({register | bx = (dealHL register.bx (highBit,lowBit))},memory)
                            else if strReg == "CX" then
                                ({register | cx = (dealHL register.cx (highBit,lowBit))},memory)
                            else
                                ({register | dx = (dealHL register.dx (highBit,lowBit))},memory)
            Err err ->
                ({register | err = "ERROR: Hexadecimal conversion decimal failure"},Memory.init)             

intToHex sign num =
    if sign == "INC" then
        "000" ++ Hex.toString (num + 1)
    else
        "000" ++ Hex.toString (num - 1)

pushDataReg register memory stack valueList tuple  =   
    if (List.member tuple valueList) then
        (register,memory)
    else
        let
            newPush = Dict.insert stack (List.append valueList [tuple]) memory.newPush      
        in    
            (register,{memory | newPush = newPush})

dealHexOrDecimal register memory second third =
    if (List.member second memory.stack) then
        let
            tuple = isHexOrDecimal third
        in
            if tuple == ("ERROR","ERROR") then
                ({register | err = "ERROR: Data is not decimal or hexadecimal"},Memory.init)
            else
                let
                    h = Tuple.first tuple
                    l = Tuple.second tuple
                    valueList = Memory.dictKey_Value (Dict.get second memory.newPush)
                in
                    pushDataReg register memory second valueList (h,l)    
    else
        if (List.member second ["AX","AH","AL","BX","BH","BL","CX","CH","CL","DX","DH","DL"]) then
            let
                tuple = isHexOrDecimal third
            in
                if tuple == ("ERROR","ERROR") then
                    ({register | err = "ERROR: Data is not decimal or hexadecimal"},Memory.init)
                else
                    let
                        h = Tuple.first tuple
                        l = Tuple.second tuple
                    in
                        whichRegister register memory second h l    
        else
           ({register | err = "Register or stack does not exist"},Memory.init)       

isHexOrDecimal third =
    let
        lowThird = String.toLower third --Hex.fromString 识别不了大写字母         
    in
        if (String.endsWith "h" lowThird) then
            let
                hexVal = "0000" ++ (String.dropRight 1 lowThird)
                msg = Hex.fromString hexVal
            in
                case msg of 
                    Ok msg1 ->
                        let
                            l = String.right 2 hexVal
                            h = String.right 2 (String.dropRight 2 hexVal)                                                  
                        in
                            (h,l)                   
                    Err _ ->
                        ("ERROR","ERROR")
        else
            let
                msg = String.toInt lowThird             
            in
                case msg of
                    Ok msg1 ->
                        let
                            hexVal = "0000" ++ (Hex.toString msg1) 
                            l = String.right 2 hexVal
                            h = String.right 2 (String.dropRight 2 hexVal)                          
                        in
                            (h,l)                                              
                    Err _ ->
                        ("ERROR","ERROR")

whichRegister register memory second h l  = 
    if second == "AX" then
        ({register | ax = (dealHL register.ax (h,l))},memory)
    else if second == "AL" then
        ({register | ax = (dealL register.ax l)},memory)
    else if second == "AH" then
        ({register | ax = (dealH register.ax l)},memory)
    else if second == "BX" then
        ({register | bx = (dealHL register.bx (h,l))},memory)
    else if second == "BL" then
        ({register | bx = (dealL register.bx l)},memory)
    else if second == "BH" then
        ({register | bx = (dealH register.bx l)},memory)
    else if second == "CX" then
        ({register | cx = (dealHL register.cx (h,l))},memory)
    else if second == "CL" then
        ({register | cx = (dealL register.cx l)},memory)
    else if second == "CH" then
        ({register | cx = (dealH register.cx l)},memory)
    else if second == "DX" then
        ({register | dx = (dealHL register.dx (h,l))},memory)
    else if second == "DL" then
        ({register | dx = (dealL register.dx l)},memory)
    else
        ({register | dx = (dealH register.dx l)},memory)

   
                    

            






