module Memory exposing (..)
import Html exposing (..)
import Dict
import Html.Attributes exposing (..)
import Material.Table as Table

type alias Model = 
    { stack : List String
    , newPush : Dict.Dict String (List (String,String))
    , pop : List Int

    }

init =
  (Model ["SS"] (Dict.fromList [("SS",[])]) [])

view model =
    div []  
      ( model.stack |> List.map (\item ->
        let
          value = dictKey_Value (Dict.get item model.newPush)  
        in
          if item == "SS" && (List.isEmpty value == True) then
            div [][] 
          else
            div [style [("display", "inline-block"),("margin", "12px")]] 
              [(text item),
                Table.table []
                  [ Table.thead []
                    [ Table.tr []
                      [ Table.td [ Table.numeric ] [ text "Stack bottom" ]
                      ]
                    , Table.tr []
                      [ Table.td [ Table.numeric ] [ text "H" ]
                      , Table.td [ Table.numeric ] [ text "L" ]
                      ]
                    ]
                    ,Table.tbody []
                      ( value |> List.map (\item_1 ->
                           Table.tr []
                             [ Table.td [ Table.numeric ] [ text (Tuple.first item_1)]
                             , Table.td [ Table.numeric ] [ text (Tuple.second item_1)]
                             ]
                         )
                      )
                  ]
              ]
        )
      )

divStyle = 
  style 
    [ ("float","left")
    , ("box-shadow","4px 4px 10px #888")
    , ("margin","10px")
    ]

dictKey_Value msg =
    case msg of 
        Just x ->
            x
        Nothing ->
            []
