module UI exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Material.Table as Table
import Dict

type alias Model =
    { allRegistValue : Dict.Dict String String }

init = (Model default)

default = 
  (Dict.fromList 
    [ ("AH", "00")
    , ("AL", "00")
    , ("BH", "00")
    , ("BL", "00")
    , ("CH", "00")
    , ("CL", "00")
    , ("DH", "00")
    , ("DL", "00")
    , ("CS", "00")
    , ("IP", "00")
    , ("SS", "00")
    , ("SP", "00")
    , ("BP", "00")
    , ("SI", "00")
    , ("DI", "00")
    , ("DS", "00")
    , ("ES", "00")
    , ("ERROR", "")
    ]
  )   

view model =
  div[]
    [ div[ divStyle ] 
      [    
        Table.table []
          [ Table.thead []
            [ Table.tr []
                [ Table.th [ Table.numeric ] [ text "Register" ]
                , Table.th [ Table.numeric ] [ text "H" ]
                , Table.th [ Table.numeric ] [ text "L" ]
                ]
            ]
            , Table.tbody []
                ((data model) |> List.map(\item ->
                    Table.tr []
                      [ Table.td [ Table.numeric ] [ text item.register ]
                      , Table.td [ Table.numeric ] [ text item.highBit ]
                      , Table.td [ Table.numeric ] [ text item.lowBit ]
                      ]
                  )
                )
          ]
      ]
    , div[ divStyle ] 
        [
          Table.table []
            [
              Table.tbody []
                ((data1 model) |> List.map(\item ->
                    Table.tr []
                      [ Table.td [ Table.numeric ] [ text item.register ]
                      , Table.td [ Table.numeric ] [ text item.highBit ]
                      ]
                   )
                )
            ]
        ]
    ]

getDict msg dict= 
    Maybe.withDefault "00" (Dict.get msg dict)

data model =
  [ { register = "AX", highBit = (getDict "AH" model.allRegistValue), lowBit = (getDict "AL" model.allRegistValue) }
  , { register = "BX", highBit = (getDict "BH" model.allRegistValue), lowBit = (getDict "BL" model.allRegistValue) }
  , { register = "CX", highBit = (getDict "CH" model.allRegistValue), lowBit = (getDict "CL" model.allRegistValue) }
  , { register = "DX", highBit = (getDict "DH" model.allRegistValue), lowBit = (getDict "DL" model.allRegistValue) }
  ]

data1 model =
  [ { register = "CS", highBit = (getDict "CS" model.allRegistValue) }
  , { register = "IP", highBit = (getDict "IP" model.allRegistValue) }
  , { register = "SS", highBit = (getDict "SS" model.allRegistValue) }
  , { register = "SP", highBit = (getDict "SP" model.allRegistValue) }
  , { register = "BP", highBit = (getDict "BP" model.allRegistValue) }
  , { register = "SI", highBit = (getDict "SI" model.allRegistValue) }
  , { register = "DI", highBit = (getDict "DI" model.allRegistValue) }
  , { register = "DS", highBit = (getDict "DS" model.allRegistValue) }
  , { register = "ES", highBit = (getDict "ES" model.allRegistValue) }
  ]

divStyle =
  style
    [ ("float", "left")
    , ("box-shadow", "4px 4px 10px #888")
    ]





