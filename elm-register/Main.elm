import Html exposing (..)
import Html.Attributes exposing (..)
import Regex
import Dict
import Tuple3
import Material
import Material.Scheme
import Material.Table as Table
import Material.Icon as Icon
import Material.Menu as Menu
import Material.Button as Button
import Material.Textfield as Textfield
import Material.Options as Options exposing (css)
import UI
import Stack
import Analysis

main = 
    Html.program
    { init = init
    , view = view
    , update = update
    , subscriptions = always Sub.none
    }

type alias Model = 
    { error : String --控制Step和Run按钮的显示
    , input : String --接收输入框内容
    , compileUiModel : UI.Model --预编译内容
    , compileStackModel : Stack.Model --预编译内容
    , uiModel : UI.Model --预编译成功之后要显示内容
    , stackModel : Stack.Model --预编译成功之后要显示内容
    , pedometer : Int --计步器
    , pedometerList : List ( (Dict.Dict String String), Stack.Model, String) --每行代码执行结果
    , codeStrList : List String --存储单步执行代码或者Run执行的代码
    , mdl : Material.Model
    }

init : (Model, Cmd Msg)
init = (default, Cmd.none)

default =
    { error = "" 
    , input = ""
    , compileUiModel = UI.init
    , compileStackModel = Stack.init
    , uiModel = UI.init
    , stackModel = Stack.init
    , pedometer = -1
    , pedometerList = []
    , codeStrList = []
    , mdl = Material.model
    }

type  Msg
    = Input String
    | ClickStep 
    | ClickRun
    | Compile
    | Mdl (Material.Msg Msg)

update : Msg -> Model -> (Model, Cmd Msg)
update msg model = 
    case msg of
        Input str ->
            ({default | input = str}, Cmd.none)
        ClickStep ->
            (clickStep model)                         
        ClickRun ->
            ({model | uiModel = model.compileUiModel, stackModel = model.compileStackModel, pedometer = -1, codeStrList = (getCodeStrList model.pedometerList)}, Cmd.none)
        Compile ->
            let
                input = model.input
                model_1 = {default | input = input}       
            in        
                (compile model_1)
        Mdl msg_ ->
            Material.update Mdl msg_ model

view : Model -> Html Msg
view model =
    div[]
        [ div[ style [("float", "left"), ("width", "23%"), ("padding", "2rem")] ] 
            [ Textfield.render Mdl [0] model.mdl
                [ Textfield.floatingLabel
                , Options.onInput Input
                , Textfield.textarea
                , Textfield.rows 16
                ]
                []
            , div []
                [ div[ style [("float", "left")] ] [(viewButton model)]
                , div[ style [("float", "left")] ]
                    [ Button.render Mdl [1] model.mdl
                        [ Button.raised
                        , Button.ripple
                        , Options.onClick Compile
                        ]
                        [ Icon.i "toys", text "Compile"] 
                    ]
                ]
            ]
        , div [ infoStyle ] [ (viewInfo model) ]
        ]
        |> Material.Scheme.top
 
viewButton : Model -> Html Msg
viewButton model =
    if model.error == "True" then   
        div [ style [("float", "left")] ] 
            [ div [ style [("float", "left"), ("margin-right", "1rem")] ]
                [ Button.render Mdl [2] model.mdl
                    [ Button.raised
                    , Button.ripple
                    , Options.onClick ClickStep
                    ]
                    [ Icon.i "skip_next", text "Step" ]
                ]
            , div [ style [("float", "left"), ("margin-right", "1rem")] ]
                [ Button.render Mdl [3] model.mdl 
                    [ Button.raised
                    , Button.ripple
                    , Options.onClick ClickRun
                    ]
                    [ Icon.i "play_arrow", text "Run" ]
                ]
            ]
    else
        div [] []

viewInfo : Model -> Html Msg
viewInfo model =
    if model.error == "True" then
        if model.codeStrList /= [] then
            div [ style [("margin-left", "6px")] ]
                [ div [ style [("float", "left"), ("width", "11%")] ][ text "执行流程：", div [ codeStyle ] (model.codeStrList |> List.map(\item -> div [ style[("white-space", "nowrap")] ] [text item])) ]
                , div [ style [("float", "left"), ("width", "32%")] ] [ UI.view model.uiModel ]
                , div [ outsideStyle ] [ div [ insideStyle ] [ Stack.view model.stackModel ] ]
                ]
        else
            div [] []
    else
        div [ style [("margin-left", "6px")] ] [ text model.error ]

dealInput : String -> (String, Dict.Dict String String)
dealInput input =
    let
        newStr = (String.lines (String.toUpper input))
                    |> List.filterMap deleteNull
                    |> List.map(\item ->
                            (String.split " " item)
                                |> List.filterMap deleteNull
                                |> String.join " "
                        )
                    |> String.join "\n"
    in
        if (String.startsWith "MAIN:\n" newStr) && (String.endsWith "\nRET" newStr) then
            let
                list = String.split "\nRET\n" (newStr ++ "\n")      
            in    
                if (Maybe.withDefault "Error" (List.head (List.reverse list))) == "" then
                    let
                        headValue = 
                            (Maybe.withDefault "" (List.head list))
                                |> regexSplit
                                |> List.drop 1
                                |> String.concat

                        delHeaderTail = List.drop 1 (List.take ((List.length list) - 1) list)

                        filterList str =
                            if (String.contains ":" str) then
                                let
                                    splitList = regexSplit str
                                    numFun = (Maybe.withDefault "" (List.head splitList))
                                                |> filterNumFun splitList         
                                in
                                    case (String.toInt numFun) of
                                        Ok intNum ->
                                           (numFun,String.concat (List.drop 1 splitList)) 
                                        Err _ ->
                                            ("","")
                            else
                                ("","")

                        filterNumFun splitList str =
                            if (List.length splitList) == 1 then
                                if (String.contains ":" str) then
                                    String.dropRight 1 str
                                else
                                    ""
                            else
                                str

                        regexSplit str =
                            Regex.split (Regex.AtMost 1) (Regex.regex ":\n") str

                        listTuple = List.map filterList delHeaderTail                                  
                    in
                        if (List.member ("","") listTuple) then
                            ("Error: Error in call function!!!", Dict.empty)
                            
                        else
                            ("", Dict.fromList (("MAIN", headValue) :: listTuple))
                            
                else
                    ("Error: End of more than two ret in a row!!!", Dict.empty)
                    
        else
            ("Error: Function header and footer format is incorrect!!!", Dict.empty)

compile : Model -> (Model, Cmd Msg)
compile model =
    let    
        (error, message) = dealInput model.input
    in
        if (Dict.isEmpty message) then
            ({model | error = error}, Cmd.none)            
        
        else
            let
                (allRegistValue, compileStackModel, pedometerList) = (Analysis.mainFun message UI.default Stack.init model.pedometerList)
                errorDict = Maybe.withDefault "" (Dict.get "ERROR" allRegistValue)
            in    
                if errorDict == "" then
                    let
                        compileUiModel = model.compileUiModel
                        compileUiModel_1 = {compileUiModel | allRegistValue = allRegistValue}           
                    in
                        ({model | compileUiModel = compileUiModel_1, compileStackModel = compileStackModel, pedometerList = pedometerList, error = "True"}, Cmd.none)
                
                else
                    ({model | error = errorDict}, Cmd.none)

getCodeStrList : List ((Dict.Dict String String), Stack.Model, String) -> List String
getCodeStrList pedometerList =
    let
        append codeStr list =
            List.append list [(Tuple3.third codeStr)]
    in
        List.foldl append [] pedometerList 

clickStep : Model -> (Model, Cmd Msg)            
clickStep model =
    let
        len = List.length model.pedometerList

        getNum pedometer len =
            if pedometer < len then
                pedometer
            else
                0

        pedometer = getNum (model.pedometer + 1) len
        dict = Dict.fromList (List.indexedMap (,) model.pedometerList)
        dictValue = Dict.get pedometer dict      
    in 
        case dictValue of
            Just tuple3 ->
                let
                    uiModel = model.uiModel
                    uiModel_1 = {uiModel | allRegistValue = (Tuple3.first tuple3)}     
                in        
                    ({model | uiModel = uiModel_1, stackModel = (Tuple3.second tuple3), pedometer = pedometer, codeStrList = [(Tuple3.third tuple3)]}, Cmd.none)
            Nothing ->
                (model, Cmd.none) 
  
deleteNull : String -> Maybe String
deleteNull str =
    if str == "" then
        Nothing
    else
        Just str

infoStyle = 
    style 
        [ ("border", "1px solid #333333")
        , ("background-color", "#cccccc")
        , ("height", "450px")
        , ("width", "72%")
        , ("float", "left")
        ]

outsideStyle =
    style 
        [ ("float", "left")
        , ("width", "56%")
        , ("margin-right", "10px")
        , ("height", "420px")
        , ("overflow", "hidden")
        ] 

insideStyle = 
    style 
        [ ("overflow", "scroll")
        , ("border", "1px solid #333333")
        , ("height", "100%")
        , ("width", "700px")
        ]

codeStyle = 
    style 
        [ ("height", "420px")
        , ("width", "100px")
        , ("overflow", "scroll")
        , ("border", "1px solid #333333")
        ]