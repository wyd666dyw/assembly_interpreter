module Analysis exposing (..)
import UI
import Stack
import Dict
import Hex
import Debug

dataRegister = ["AX", "AH", "AL", "BX", "BH", "BL", "CX", "CH", "CL", "DX", "DH", "DL"]
eight_Bit_Register = ["AH", "AL", "BH", "BL", "CH", "CL", "DH", "DL"]
sixteen_Bit_Register = ["AX", "BX", "CX", "DX"]

mainFun : 
        Dict.Dict String String 
        -> Dict.Dict String String 
        -> Stack.Model 
        -> List ((Dict.Dict String String), Stack.Model, String)
        -> (Dict.Dict String String, Stack.Model, List ((Dict.Dict String String), Stack.Model, String))

mainFun message allRegistValue stackModel pedometerList =
    let
        uiStack = (analysis "MAIN" allRegistValue stackModel message pedometerList)      
    in
        (retnResult uiStack)

callFun : 
        String 
        -> Dict.Dict String String 
        -> Stack.Model 
        -> Dict.Dict String String 
        -> List ((Dict.Dict String String), Stack.Model, String)
        -> (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String))

callFun second allRegistValue stackModel message pedometerList =
    let
        toInt = String.toInt second        
    in
        case toInt of
            Ok num ->
                (analysis second allRegistValue stackModel message pedometerList)
            Err _ ->
                (allRegistValue, stackModel, message, pedometerList) 

analysis : 
        String 
        -> Dict.Dict String String 
        -> Stack.Model 
        -> Dict.Dict String String
        -> List ((Dict.Dict String String), Stack.Model, String) 
        -> (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String))

analysis callMain allRegistValue stackModel message pedometerList =   
    let
        info = dictValue callMain message       
    in
        if info == "" then
            (allRegistValue, stackModel, message, pedometerList)

        else
            let
                infoList = String.split "\n" info
            in
               List.foldl (singleLineCode) (allRegistValue, stackModel, message, pedometerList) infoList

retnResult : 
        (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String)) 
        -> (Dict.Dict String String, Stack.Model, List ((Dict.Dict String String), Stack.Model, String))

retnResult (allRegistValue, stackModel, _, pedometerList) = 
    let
        errValue = Maybe.withDefault "" (Dict.get "ERROR" allRegistValue)       
    in       
        if errValue == "" then
            (allRegistValue, stackModel, pedometerList)
        
        else  --结果错误，保留错误内容其他所有已更改内容初始化
            let 
                allRegistValue_1 = UI.default  
            in
                ((Dict.update "ERROR" (\_ -> Just errValue) allRegistValue_1), Stack.init, []) 

dictValue : 
        String 
        -> Dict.Dict String String 
        -> String

dictValue key dict =
    Maybe.withDefault "" (Dict.get key dict)

singleLineCode : 
        String 
        -> (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String)) 
        -> (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String)) 

singleLineCode codeStr (allRegistValue, stackModel, message, pedometerList) =
    let
        strList = String.split " " codeStr
        len = List.length strList  
        dict = Dict.fromList (List.indexedMap (,) strList)   
    in
        if len >= 2 && len <= 3 then
            let
                first = Maybe.withDefault "" (Dict.get 0 dict)
                second = Maybe.withDefault "" (Dict.get 1 dict)           
            in
                if len == 2 then
                    if first == "INC" || first == "DEC" then
                        if (List.member second dataRegister) then
                            let
                                l = String.left 1 second
                                r = String.right 1 second
                                (hexString, sign) = 
                                    if r == "X" then
                                        ((dictValue (l ++ "H") allRegistValue) ++ (dictValue (l ++ "L") allRegistValue), l)
                                    else
                                        ((dictValue second allRegistValue), second)
                            in
                                (incOrDec first (hexString, sign) allRegistValue stackModel message pedometerList codeStr)
                        
                        else
                          ((Dict.update "ERROR" (\_ -> Just "ERROR: INC or DEC parameter error") allRegistValue), stackModel, message, pedometerList)    

                    else if first == "PUSH" then
                        if (List.member second sixteen_Bit_Register) then
                            let
                                valueList = Maybe.withDefault [] (Dict.get "SS" stackModel.newPush)
                                l = String.left 1 second
                                tupleValue = ((dictValue (l ++ "H") allRegistValue), (dictValue (l ++ "L") allRegistValue))   
                            in
                                if (List.member tupleValue valueList) then
                                    ((Dict.update "ERROR" (\_ -> Just "ERROR: Data already exists in stack") allRegistValue), stackModel, message, pedometerList)
                                
                                else
                                    let
                                        newPush = Dict.insert "SS" (List.append valueList [tupleValue]) stackModel.newPush 
                                        stackModel_1 = {stackModel | newPush = newPush}    
                                    in 
                                        (allRegistValue, stackModel_1, message, (List.append pedometerList [(allRegistValue, stackModel_1, codeStr)]))

                        else
                            if (List.member second stackModel.stack) then
                                ((Dict.update "ERROR" (\_ -> Just "ERROR: The stack already exists") allRegistValue), stackModel, message, pedometerList)
                            
                            else
                                let
                                    stack = List.append stackModel.stack [second] 
                                    newPush = Dict.insert second [] stackModel.newPush 
                                    stackModel_1 = {stackModel | stack = stack, newPush = newPush}      
                                in       
                                    (allRegistValue, stackModel_1, message, (List.append pedometerList [(allRegistValue, stackModel_1, codeStr)]))

                    else if first == "POP" then
                        if (List.member second sixteen_Bit_Register) then
                            let
                                valueList = Maybe.withDefault [] (Dict.get "SS" stackModel.newPush)      
                            in
                                if (List.isEmpty valueList) then
                                    ((Dict.update "ERROR" (\_ -> Just "ERROR: No data in the stack") allRegistValue), stackModel, message, pedometerList)  
                                
                                else
                                    let
                                        value = List.head (List.reverse valueList)       
                                    in
                                        case value of
                                            Just x ->
                                                let
                                                    y = List.take ((List.length valueList) - 1) valueList
                                                    newPush = Dict.insert "SS" y stackModel.newPush 
                                                    l = String.left 1 second
                                                    allRegistValue_1 = Dict.update (l ++ "L") (\_ -> Just (Tuple.second x)) allRegistValue 
                                                    allRegistValue_2 = Dict.update (l ++ "H") (\_ -> Just (Tuple.first x)) allRegistValue_1
                                                    stackModel_1 = {stackModel | newPush = newPush}
                                                in    
                                                    (allRegistValue_2, stackModel_1, message, (List.append pedometerList [(allRegistValue_2, stackModel_1, codeStr)])) 
                                            Nothing ->
                                                (allRegistValue, stackModel, message, pedometerList)

                        else  
                            let
                                valueList = Maybe.withDefault [] (Dict.get second stackModel.newPush)                          
                            in
                                if (List.isEmpty valueList) then
                                    ((Dict.update "ERROR" (\_ -> Just "ERROR: No stack or no data") allRegistValue), stackModel, message, pedometerList)  
                                
                                else
                                    let
                                        y = List.take ((List.length valueList) - 1) valueList  
                                        newPush = Dict.insert second y stackModel.newPush
                                        stackModel_1 = {stackModel | newPush = newPush}     
                                    in 
                                        (allRegistValue, stackModel_1, message, (List.append pedometerList [(allRegistValue, stackModel_1, codeStr)]))                                                                                                                     

                    else if first == "CALL" then
                        (callFun second allRegistValue stackModel message pedometerList)

                    else if first == "MUL" || first == "DIV" then
                        if (List.member second dataRegister) then
                            if (List.member second sixteen_Bit_Register) then
                                let
                                    l = String.left 1 second
                                    srcValue = (dictValue (l ++ "H") allRegistValue) ++ (dictValue (l ++ "L") allRegistValue)
                                    temp = (dictValue "AH" allRegistValue) ++ (dictValue "AL" allRegistValue)
                                    temp_1 = (dictValue "DH" allRegistValue) ++ (dictValue "DL" allRegistValue) 
                                    dstValue =
                                        if first == "MUL" then
                                            temp
                                        else
                                            temp_1 ++ temp
                                in
                                    (mulOrDiv first l dstValue srcValue allRegistValue stackModel message pedometerList codeStr)
                                       
                            else
                                let
                                    srcValue = (dictValue second allRegistValue)
                                    temp = (dictValue "AL" allRegistValue) 
                                    dstValue =
                                        if first == "MUL" then
                                            temp
                                        else
                                            (dictValue "AH" allRegistValue) ++ temp             
                                in
                                    (mulOrDiv first second dstValue srcValue allRegistValue stackModel message pedometerList codeStr)
                                    
                        else
                            ((Dict.update "ERROR" (\_ -> Just "ERROR: Source operand error for MUL or DIV") allRegistValue), stackModel, message, pedometerList)

                    else
                        
                        ((Dict.update "ERROR" (\_ -> Just "ERROR: Wrong sink parameter type") allRegistValue), stackModel, message, pedometerList)
                
                else
                    let
                        third = Maybe.withDefault "" (Dict.get 2 dict)     
                    in
                        if first == "MOV" then
                            if (List.member second dataRegister) then
                                if (List.member second sixteen_Bit_Register) then       
                                    if (List.member third sixteen_Bit_Register) then
                                        let
                                            sl = String.left 1 second 
                                            tl = String.left 1 third
                                            thirdLow = dictValue (tl ++ "L") allRegistValue
                                            thirdHigh = dictValue (tl ++ "H") allRegistValue
                                            allRegistValue_1 = Dict.update (sl ++ "L") (\_ -> Just thirdLow) allRegistValue
                                            allRegistValue_2 = Dict.update (sl ++ "H") (\_ -> Just thirdHigh) allRegistValue_1       
                                        in
                                            (allRegistValue_2, stackModel, message, (List.append pedometerList [(allRegistValue_2, stackModel, codeStr)])) 
                                                
                                    else
                                        let
                                            (thirdHigh, thirdLow) = (isHexOrDecimal third)        
                                        in
                                            if (thirdHigh, thirdLow) /= ("ERROR", "ERROR") then
                                                let
                                                    sl = String.left 1 second 
                                                    allRegistValue_1 = Dict.update (sl ++ "L") (\_ -> Just thirdLow) allRegistValue 
                                                    allRegistValue_2 = Dict.update (sl ++ "H") (\_ -> Just thirdHigh) allRegistValue_1      
                                                in     
                                                    (allRegistValue_2, stackModel, message, (List.append pedometerList [(allRegistValue_2, stackModel, codeStr)])) 
                                            else
                                                ((Dict.update "ERROR" (\_ -> Just "ERROR: Immediate number is not decimal or hexadecimal in 16bit register") allRegistValue), stackModel, message, pedometerList)

                                else
                                    if (List.member third eight_Bit_Register) then
                                        let
                                            allRegistValue_1 = Dict.update second (\_ -> Just (dictValue third allRegistValue)) allRegistValue        
                                        in       
                                            (allRegistValue_1, stackModel, message, (List.append pedometerList [(allRegistValue_1, stackModel, codeStr)])) 
                                                
                                    else
                                        let
                                            (thirdHigh, thirdLow) = (isHexOrDecimal third)        
                                        in
                                            if (thirdHigh, thirdLow) /= ("ERROR", "ERROR") then
                                                let
                                                    allRegistValue_1 = Dict.update second (\_ -> Just thirdLow) allRegistValue       
                                                in        
                                                    (allRegistValue_1, stackModel, message, (List.append pedometerList [(allRegistValue_1, stackModel, codeStr)]))
                                            else
                                                ((Dict.update "ERROR" (\_ -> Just "ERROR: Immediate number is not decimal or hexadecimal in 8bit register") allRegistValue), stackModel, message, pedometerList)
                        
                            else
                                if (List.member second stackModel.stack) then
                                    let
                                        tupleValue = (isHexOrDecimal third)      
                                    in
                                        if tupleValue /= ("ERROR", "ERROR") then
                                            let
                                                valueList = Maybe.withDefault [] (Dict.get second stackModel.newPush)      
                                            in
                                                if (List.member tupleValue valueList) then
                                                    ((Dict.update "ERROR" (\_ -> Just "ERROR: This value already exists in the stack") allRegistValue), stackModel, message, pedometerList)
                                                
                                                else
                                                    let
                                                        newPush = Dict.insert second (List.append valueList [tupleValue]) stackModel.newPush 
                                                        stackModel_1 = {stackModel | newPush = newPush}     
                                                    in 
                                                        (allRegistValue, stackModel_1, message, (List.append pedometerList [(allRegistValue, stackModel_1, codeStr)]))
    
                                        else
                                            ((Dict.update "ERROR" (\_ -> Just "ERROR: Immediate number is not decimal or hexadecimal in stack") allRegistValue), stackModel, message, pedometerList)

                                else 
                                    ((Dict.update "ERROR" (\_ -> Just "ERROR: Register or stack does not exist") allRegistValue), stackModel, message, pedometerList)
                        else if first == "ADD" || first == "SUB" then
                            if (List.member second dataRegister) then
                                if (List.member second sixteen_Bit_Register) then      
                                    if (List.member third sixteen_Bit_Register) then
                                        let
                                            tl = String.left 1 third 
                                            srcValue = (dictValue (tl ++ "H") allRegistValue) ++ (dictValue (tl ++ "L") allRegistValue) 
                                            sl = String.left 1 second
                                            dstValue = (dictValue (sl ++ "H") allRegistValue) ++ (dictValue (sl ++ "L") allRegistValue)
                                        in
                                            (addOrSub first sl dstValue srcValue allRegistValue stackModel message pedometerList codeStr)

                                    else
                                        let
                                            (srcHighBit, srcLowBit) = (isHexOrDecimal third)        
                                        in
                                            if (srcHighBit, srcLowBit) /= ("ERROR", "ERROR") then
                                                let
                                                    sl = String.left 1 second
                                                    dstValue = (dictValue (sl ++ "H") allRegistValue) ++ (dictValue (sl ++ "L") allRegistValue)           
                                                in       
                                                    (addOrSub first sl dstValue (srcHighBit ++ srcLowBit) allRegistValue stackModel message pedometerList codeStr)
                                       
                                            else
                                                ((Dict.update "ERROR" (\_ -> Just "ERROR: Immediate number is not decimal or hexadecimal in 16bit register") allRegistValue), stackModel, message, pedometerList)

                                else
                                    if (List.member third eight_Bit_Register) then
                                        let
                                            srcValue = (dictValue third allRegistValue)
                                            dstValue = (dictValue second allRegistValue)       
                                        in
                                            (addOrSub first second dstValue srcValue allRegistValue stackModel message pedometerList codeStr)
                                    
                                    else
                                        let
                                            (srcHighBit, srcLowBit) = (isHexOrDecimal third)        
                                        in
                                            if (srcHighBit, srcLowBit) /= ("ERROR", "ERROR") then
                                                let
                                                    dstValue = (dictValue second allRegistValue)      
                                                in       
                                                    (addOrSub first second dstValue (srcHighBit ++ srcLowBit) allRegistValue stackModel message pedometerList codeStr)
                                       
                                            else
                                                ((Dict.update "ERROR" (\_ -> Just "ERROR: Immediate number is not decimal or hexadecimal in 16bit register") allRegistValue), stackModel, message, pedometerList)
                                    
                            else
                                ((Dict.update "ERROR" (\_ -> Just "ERROR: Destination operand error for ADD or SUB") allRegistValue), stackModel, message, pedometerList)
                        
                        else                            
                            ((Dict.update "ERROR" (\_ -> Just "ERROR: Three - element parameter error") allRegistValue), stackModel, message, pedometerList)
        
        else
            ((Dict.update "ERROR" (\_ -> Just "ERROR: Wrong number of parameters") allRegistValue), stackModel, message, pedometerList)   

incOrDec 
        : String 
        -> (String, String) 
        -> Dict.Dict String String 
        -> Stack.Model 
        -> Dict.Dict String String 
        -> List ((Dict.Dict String String), Stack.Model, String)
        -> String
        -> (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String))

incOrDec first (hexString, sign) allRegistValue stackModel message pedometerList codeStr =
    let
        hexToInt = Hex.fromString hexString              
    in
        case hexToInt of
            Ok num ->
                let
                    hexValue = 
                        if first == "INC" then
                            "000" ++ Hex.toString (num + 1)
                        else
                            if (num - 1) > 0 then
                                "000" ++ Hex.toString (num - 1) 
                            else
                                case (Hex.fromString ("1" ++ hexString)) of
                                    Ok decNum ->
                                        "000" ++ Hex.toString (decNum - 1)
                                    Err _ ->
                                        "0000"
                    putLowBit = String.right 2 hexValue
                    allRegistValue_2 =
                        if ((String.length sign) == 1) then
                            let
                                putHighBit = String.right 2 (String.dropRight 2 hexValue)
                                allRegistValue_1 = Dict.update (sign ++ "L") (\_ -> Just putLowBit) allRegistValue        
                            in
                                Dict.update (sign ++ "H") (\_ -> Just putHighBit) allRegistValue_1
                        
                        else
                            Dict.update sign (\_ -> Just putLowBit) allRegistValue
                in
                    (allRegistValue_2, stackModel, message, (List.append pedometerList [(allRegistValue_2, stackModel, codeStr)]))                               
            Err _ ->
                ((Dict.update "ERROR" (\_ -> Just "ERROR: Hex.fromString error") allRegistValue), stackModel, message, pedometerList)    

isHexOrDecimal : 
        String 
        -> (String, String)

isHexOrDecimal third =
    let
        lowThird = String.toLower third --Hex.fromString 识别不了大写字母         
    in
        if (String.endsWith "h" lowThird) then
            let
                hexVal = "0000" ++ (String.dropRight 1 lowThird)
            in
                case (Hex.fromString hexVal) of 
                    Ok _ ->
                        let
                            l = String.right 2 hexVal
                            h = String.right 2 (String.dropRight 2 hexVal)                                                  
                        in
                            (h,l)                   
                    Err _ ->
                        ("ERROR","ERROR")
        else
            case (String.toInt lowThird) of
                Ok num ->
                    let
                        hexVal = "0000" ++ (Hex.toString num) 
                        l = String.right 2 hexVal
                        h = String.right 2 (String.dropRight 2 hexVal)                          
                    in
                        (h,l)                                              
                Err _ ->
                    ("ERROR","ERROR")

addOrSub:
        String
        -> String
        -> String 
        -> String
        -> Dict.Dict String String 
        -> Stack.Model 
        -> Dict.Dict String String
        -> List ((Dict.Dict String String), Stack.Model, String)
        -> String 
        -> (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String))

addOrSub first sign dstValue srcValue allRegistValue stackModel message pedometerList codeStr =
    case (Hex.fromString dstValue) of
        Ok dstNum ->
            case (Hex.fromString srcValue) of
                Ok srcNum ->
                    let
                        dstHexValue = 
                            if first == "ADD" then
                                "000" ++ Hex.toString (dstNum + srcNum)
                            else
                                if (dstNum - srcNum) > 0 then
                                    "000" ++ Hex.toString (dstNum - srcNum)
                                else
                                    case (Hex.fromString ("1" ++ dstValue)) of
                                        Ok newDstNum ->
                                            "000" ++ Hex.toString (newDstNum - srcNum)
                                        Err _ ->
                                            "0000"
                        dstLowBit = String.right 2 dstHexValue
                        allRegistValue_2 = 
                            if ((String.length sign) == 1) then
                                let
                                    dstHighBit = String.right 2 (String.dropRight 2 dstHexValue)
                                    allRegistValue_1 = Dict.update (sign ++ "L") (\_ -> Just dstLowBit) allRegistValue        
                                in
                                    Dict.update (sign ++ "H") (\_ -> Just dstHighBit) allRegistValue_1 
                            
                            else
                                Dict.update sign (\_ -> Just dstLowBit) allRegistValue
                    in   
                        (allRegistValue_2, stackModel, message, (List.append pedometerList [(allRegistValue_2, stackModel, codeStr)]))                                                  
                Err _ ->
                    ((Dict.update "ERROR" (\_ -> Just "ERROR: Hex.fromString error") allRegistValue), stackModel, message, pedometerList)
        Err _ ->
            ((Dict.update "ERROR" (\_ -> Just "ERROR: Hex.fromString error") allRegistValue), stackModel, message, pedometerList)  

mulOrDiv: 
        String
        -> String
        -> String
        -> String
        -> Dict.Dict String String 
        -> Stack.Model 
        -> Dict.Dict String String
        -> List ((Dict.Dict String String), Stack.Model, String)
        -> String 
        -> (Dict.Dict String String, Stack.Model, Dict.Dict String String, List ((Dict.Dict String String), Stack.Model, String))

mulOrDiv first sign dstValue srcValue allRegistValue stackModel message pedometerList codeStr =
    case (Hex.fromString dstValue) of
        Ok dstNum ->
            case (Hex.fromString srcValue) of
                Ok srcNum ->
                    if first == "MUL" then
                        let
                            productValue = "0000000" ++ Hex.toString (dstNum * srcNum)
                            lowAx = String.right 2 productValue
                            highAx = String.right 2 (String.dropRight 2 productValue)
                            allRegistValue_0 = Dict.update "AL" (\_ -> Just lowAx) allRegistValue
                            allRegistValue_1 = Dict.update "AH" (\_ -> Just highAx) allRegistValue_0
                            allRegistValue_3 =
                                if ((String.length sign) == 1) then
                                    allRegistValue_1
                               
                                else
                                    let
                                        dxValue = String.right 4 (String.dropRight 4 productValue)
                                        lowDx = String.right 2 dxValue
                                        highDx = String.dropRight 2 dxValue
                                        allRegistValue_2 = Dict.update "DL" (\_ -> Just lowDx) allRegistValue_1   
                                    in
                                        Dict.update "DH" (\_ -> Just highDx) allRegistValue_2
                        in 
                            (allRegistValue_3, stackModel, message, (List.append pedometerList [(allRegistValue_3, stackModel, codeStr)]))
                    else
                        let
                            quotientValue = "000" ++ Hex.toString (dstNum // srcNum)
                            residualValue = "000" ++ Hex.toString (dstNum % srcNum)
                            lowAx = String.right 2 quotientValue
                            highAx =
                                if ((String.length sign) == 1) then
                                    String.right 2 (String.dropRight 2 quotientValue)
                                else
                                    String.right 2 residualValue
                            allRegistValue_0 = Dict.update "AL" (\_ -> Just lowAx) allRegistValue
                            allRegistValue_1 = Dict.update "AH" (\_ -> Just highAx) allRegistValue_0
                            allRegistValue_3 = 
                                if ((String.length sign) == 1) then
                                    let
                                        lowDx = String.right 2 residualValue
                                        highDx = String.right 2 (String.dropRight 2 residualValue)
                                        allRegistValue_2 = Dict.update "DL" (\_ -> Just lowDx) allRegistValue_1        
                                    in
                                        Dict.update "DH" (\_ -> Just highDx) allRegistValue_2

                                else
                                    allRegistValue_1
                                                    
                        in
                            (allRegistValue_3, stackModel, message, (List.append pedometerList [(allRegistValue_3, stackModel, codeStr)])) 

                Err _ ->
                    ((Dict.update "ERROR" (\_ -> Just "ERROR: Hex.fromString error") allRegistValue), stackModel, message, pedometerList)
        Err _ ->
            ((Dict.update "ERROR" (\_ -> Just "ERROR: Hex.fromString error") allRegistValue), stackModel, message, pedometerList)
       

